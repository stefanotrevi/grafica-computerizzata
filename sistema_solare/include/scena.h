#pragma once

#ifndef TIME_SPEED
    #define TIME_SPEED 1.f // fattore di accelerazione del tempo
#endif

#define SOLAR_SCALE 1        // fattore di scala distanza pianeti dal sole (per 10^6 km)
#define PLANET_SCALE .0005f  // fattore di scala dimensione pianeti
#define SUN_SCALE .000025    // fattore di scala sole
#define MOON_SCALE 20        // fattore scala orbita luna
#define MOON_SPEED_SCALE 0.2 // fattore scala velocità

#define SOLAR_SIZE (10000 * SOLAR_SCALE) // dimensione inquadratura, in milioni di km
#define BG_SIZE (7000 * SOLAR_SCALE)     // dimensione sfondo, in milioni di km

#define CLOUD_HEIGHT (100 * PLANET_SCALE) // altezza nuvole, in km, rispetto al pianeta
#define CLOUD_SPEED .25f                  // velocità angolare nuvole, in rad/tick

#define RING_IN_DIST (20000 * PLANET_SCALE) // km fra la superficie e l'inizio degli anelli
#define RING_EX_DIST (70000 * PLANET_SCALE) // km fra la superficie e la fine degli anelli

#define CUBE(x) (x * x * x)

// Inizializza la finestra
void windowInit(void);

// Inizializza la scena
void sceneInit(void);

// Disegna una stringa a schermo
void drawBitmapString(void *font, const char *str);

// Aggiorna le info sugli FPS
void updateFPS(void);

#pragma once

#include "ellipsetypes.h"

// Inizializza un ellisse
ellipse_t ellipseInit(float ax_min, float ax_maj, float off, int segments);

// Elimina i dati lato host (CPU)
void ellipseFreeHost(ellipse_t *e);

// Disegna l'ellisse
void ellipseDraw(ellipse_t *e);

#pragma once

#include "ringtypes.h"

// Inizializza un anello con un dato raggio esterno/interno
ring_t ringInit(float rad_in, float rad_ext, int segments);

// Elimina i dati lato host (CPU)
void ringDelHost(ring_t *r);

// Disegna un anello
void ringDraw(ring_t *ring);

#pragma once

#include "spheretypes.h"

// Inizializza una sfera (inclusi VBO)
sphere_t sphereInit(float rad, int sect, int stack, float direction);

// Libera la memoria allocata lato host (CPU)
void sphereDelHost(sphere_t *s);

// Disegna una sfera
void sphereDraw(sphere_t *s);

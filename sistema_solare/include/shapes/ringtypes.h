#pragma once

#include <GL/glew.h>

typedef struct ring
{
    float rad_in;    // raggio interno
    float rad_ext;   // raggio esterno
    GLsizei vert_n;  // numero di vertici
    GLuint vao;      // VAO anello
    GLfloat *data;   // Contiene, nell'ordine: {x, y, z, nx, ny, nz, t, s}
    GLsizei data_sz; // numero di vertici
    GLuint vbo;      // VBO vertici
} ring_t;

#define RING_SEGMENT 360 // numero standard di segmenti per un anello

#define RING_CORD 3 // numero di coordinate
#define RING_NORM 3 // numero di normali
#define RING_TEXT 2 // numero di texture

// offset vari
#define RING_CORD_OFF ((const void *)0)
#define RING_NORM_OFF ((const void *)(RING_CORD * sizeof(GLfloat)))
#define RING_TEXT_OFF ((const void *)((RING_CORD + RING_NORM) * sizeof(GLfloat)))

// dimensione stride
#define RING_STRIDE ((RING_CORD + RING_NORM + RING_TEXT) * sizeof(GLfloat))

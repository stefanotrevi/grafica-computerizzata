#pragma once

#include <GL/glew.h>

typedef struct ellipse
{
    float ax_min;    // asse minore
    float ax_maj;    // asse maggiore
    GLsizei vert_n;  // numero di vertici
    GLfloat *data;   // array dei vertici
    GLsizei data_sz; // dimensione array dei vertici
    GLuint vao;      // VAO
    GLuint vbo;      // VBO
} ellipse_t;

#define ELLIPSE_VERTEX 360 // numero di vertici di default

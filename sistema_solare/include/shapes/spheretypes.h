#pragma once

#include <GL/glew.h>

typedef struct sphere
{
    float rad;       // raggio
    int sect;        // numero di settori
    int stack;       // numero di stack
    GLfloat *data;   // contiene, per ogni vertice: {x, y, z, nx, ny, nz, s, t}
    GLsizei data_sz; // dimensione in byte di data
    GLuint data_vbo; // VBO dei dati
    GLuint *idx;     // array degli indici
    GLsizei idx_sz;  // dimensione dell'array degli indici
    GLuint idx_vbo;  // VBO degli indici
} sphere_t;

#define SPHERE_SECT 64           // numero standard di settori per una sfera
#define SPHERE_STACK SPHERE_SECT // numero standard di stack per una sfera

#define SPHERE_CORD 3 // numero di coordinate
#define SPHERE_NORM 3 // numero di normali
#define SPHERE_TEXT 2 // numero di texture

// offset vari
#define SPHERE_CORD_OFF ((const void *)0)
#define SPHERE_NORM_OFF ((const void *)(SPHERE_CORD * sizeof(GLfloat)))
#define SPHERE_TEXT_OFF ((const void *)((SPHERE_CORD + SPHERE_NORM) * sizeof(GLfloat)))

// dimensione stride
#define SPHERE_STRIDE (SPHERE_CORD + SPHERE_NORM + SPHERE_TEXT)
#define SPHERE_BSTRIDE (SPHERE_STRIDE * sizeof(GLfloat))

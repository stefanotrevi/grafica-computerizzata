#pragma once

#include "shapes/spheretypes.h"
#include "shapes/ringtypes.h"
#include "shapes/ellipse.h"

enum solarpla
{
    PLA_MER,
    PLA_VEN,
    PLA_TER,
    PLA_MAR,
    PLA_GIO,
    PLA_SAT,
    PLA_URA,
    PLA_NET,
    PLA_NUM,
};

#define PLA0 PLA_MER

enum textures
{
    TEX_SURF,
    TEX_NORM,
    TEX_SPEC,
    TEX_NIGHT,
    TEX_CLOUD,
    TEX_RING,
    TEX_NUM,
    TEX_SURFI = GL_TEXTURE0,
    TEX_NORMI = GL_TEXTURE1,
    TEX_SPECI = GL_TEXTURE2,
    TEX_NIGHTI = GL_TEXTURE3,
    TEX_CLOUDI = GL_TEXTURE4,
    TEX_RINGI = GL_TEXTURE5,
};

struct material
{
    GLfloat shi;    // luminosità
    GLfloat col[3]; // colori (in caso non ci sia luce)
    GLfloat amb[4]; // ambientale
    GLfloat dif[4]; // diffusa
    GLfloat spe[4]; // speculare
};

struct planet_spec
{
    GLfloat dist[2];     // distanza minima e massima dal sole, in milioni di km
    GLfloat axes[2];     // semiassi minore e maggiore
    GLfloat ecc;         // eccentricità
    GLfloat off;         // offset del fuoco rispetto all'origine
    GLfloat size;        // raggio, in km
    GLfloat pos;         // posizione attuale
    GLfloat speed;       // velocità di rivoluzione
    GLfloat rot_pos;     // posizione angolare
    GLfloat rot_speed;   // velocità di rivoluzione (inserire il periodo in ore)
    GLfloat cloud_pos;   // posizione nuvole
    GLfloat cloud_speed; // velocità nuvole
};

typedef struct planet
{
    char *name;               // nome pianeta
    struct planet_spec specs; // specifiche del pianeta
    struct material mat;      // proprietà del materiale
    GLuint tex[TEX_NUM];      // Array delle texture
    ellipse_t orbit;          // ID del VBO dell'orbita
    sphere_t surf;            // superficie del pianeta
    sphere_t clouds;          // nuvole del pianeta
    ring_t ring;              // anelli del pianeta
    struct planet *moon;      // lune del pianeta
    int moon_num;             // numero di lune
} planet_t;

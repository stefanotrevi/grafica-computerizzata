#pragma once

#include <GL/glew.h>

#define MAX_FLEN 256

enum uniforms
{
    UNI_VEC3,
    UNI_MAT4,
};

// Carica una texture e ritorna l'id
GLuint loadTexture(char *fname, GLenum kind);

// Carica uno shader, e ritorna l'id
GLuint loadShader(char *fname, GLenum type);

// Carica un quadric, e ne ritorna l'indirizzo
GLUquadric *loadQuadric(void);

// Carica un vertex ed un fragment shader
GLuint loadProgram(char *f_vert, char *f_frag);

// Carica una variabile uniforme nello shader
void loadUniform(GLuint prog, const char *name, void *value, enum uniforms kind);

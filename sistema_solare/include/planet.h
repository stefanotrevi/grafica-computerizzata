#pragma once

#include "planettypes.h"

// Esegue il setup di un pianeta, non ne crea uno da zero (solitamente viene fatto a mano)
void planetSetup(planet_t *pla);

// Aggiorna le specifiche del pianeta
void planetTic(struct planet_spec *spec);

// Disegna il pianeta
void planetDraw(planet_t *pla);

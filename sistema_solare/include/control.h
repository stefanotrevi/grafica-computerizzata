#pragma once

#include "math/vectypes.h"

typedef struct camera
{
    float yaw;
    float pitch;
    vec3_t eye;
    vec3_t front;
    vec3_t up;
} camera_t;

// Stampa info sui comandi
void printUsage(void);

// Inizializza le funzioni di controllo
void ctrlInit(void);

// Posiziona la telecamera cam nella scena
void placeCamera(void);

// Gestisce il click del mouse
void mouse(int btn, int state, int x, int y);

// Gestisce il movimento del mouse
void motion(int x, int y);

// Gestisce i comandi da tastiera
void keyboard(unsigned char key, int x, int y);

#pragma once

#include "vec2.h"
#include "vec3.h"

// clang-format off
// Somma due vettori
#define sum(v1, v2) _Generic((v1), vec2_t : sum2, vec3_t : sum3)(v1, v2)

// Sottrae due vettori
#define sub(v1, v2) _Generic((v1), vec2_t : sub2, vec3_t : sub3)(v1, v2)

// Moltiplica scalarmente due vettori
#define mul(v1, v2) _Generic((v1), vec2_t : mul2, vec3_t : mul3)(v1, v2)

// Moltiplica vettorialmente due vettori
#define cross(v1, v2) _Generic((v1), vec2_t : cross2, vec3_t : cross3)(v1, v2)

// Moltiplica un vettore per uno scalare
#define smul(v, s) _Generic((v), vec2_t : smul2, vec3_t : smul3)(v, s)

// Ritorna la norma (euclidea) del vettore
#define norm(v) _Generic((v), vec2_t : norm2, vec3_t : norm3)(v)

// Normalizza un vettore
#define normalize(v) _Generic((v), vec2_t : normalize2, vec3_t : normalize3)(v)

// Normalizza il vettore tra 0 e 1
#define vto01(v) _Generic((v), vec2_t : v2to01, vec3_t : v3to01)(v)

#pragma once

// Converte gradi in radianti
float degToRad(float deg);

// Converte radianti in gradi
float radToDeg(float rad);

// Aggiorna un parametro e, se supera il massimo, lo resetta a 0
void incmax(float *f, float delta, float max);

// ritona un floating point casuale nel range [min, max-1]
float frand(float min, float max);

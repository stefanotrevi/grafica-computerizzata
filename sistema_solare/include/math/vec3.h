#pragma once

#include "vectypes.h"

// Somma due vettori
vec3_t sum3(vec3_t v1, vec3_t v2);

// Sottrae due vettori
vec3_t sub3(vec3_t v1, vec3_t v2);

// Moltiplica scalarmente due vettori
vec3_t mul3(vec3_t v1, vec3_t v2);

// Moltiplica vettorialmente due vettori
vec3_t cross3(vec3_t v1, vec3_t v2);

// Moltiplica un vettore per uno scalare
vec3_t smul3(vec3_t v, float s);

// Ritorna la norma (euclidea) del vettore
float norm3(vec3_t v);

// Normalizza un vettore
vec3_t normalize3(vec3_t v);

// Ritorna il vettore direzione dati gli angoli di Eulero
vec3_t euler3(float yaw, float pitch);

// Normalizza il vettore tra 0 e 1
vec3_t v3to01(vec3_t v);

#pragma once

#include "vectypes.h"

// Somma due vettori
vec2_t sum2(vec2_t v1, vec2_t v2);

// Sottrae due vettori
vec2_t sub2(vec2_t v1, vec2_t v2);

// Moltiplica scalarmente due vettori
vec2_t mul2(vec2_t v1, vec2_t v2);

// Moltiplica vettorialmente due vettori
vec2_t cross2(vec2_t v1, vec2_t v2);

// Moltiplica un vettore per uno scalare
vec2_t smul2(vec2_t v, float s);

// Ritorna la norma (euclidea) di un vettore
float norm2(vec2_t v);

// Normalizza un vettore
vec2_t normalize2(vec2_t v);

// Ritorna il vettore direzione dati gli angoli di Eulero
vec2_t euler2(float yaw, float pitch);

// Normalizza il vettore tra 0 e 1
vec2_t v2to01(vec2_t v);

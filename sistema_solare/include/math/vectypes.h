#pragma once

typedef struct vec2
{
    float x;
    float y;
} vec2_t;

typedef struct vec3
{
    float x;
    float y;
    float z;
} vec3_t;

typedef struct vec4
{
    float x;
    float y;
    float z;
    float w;
} vec4_t;

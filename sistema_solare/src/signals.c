#include "signals.h"
#include "scena.h"

#ifndef _WIN32
    #include <signal.h>
    #include <sys/time.h>
#endif
#include <GL/freeglut.h>
#include <stdio.h>
#include <time.h>

#ifdef _WIN32
static void sighand(HWND hand, UINT msg, UINT_PTR id, DWORD elap)
{
    (void)hand;
    (void)msg;
    (void)id;
    (void)elap;

    updateFPS();
}
#else
static void sighand(int sig)
{
    if (sig == SIGALRM)
        updateFPS();
}
#endif

void sigInit(void)
{
#ifdef _WIN32
    SetTimer(NULL, 0, 1000, sighand);
#else
    struct itimerval itv = {{.tv_sec = 1}, {.tv_sec = 1}};
    struct sigaction sa = {0};

    sigemptyset(&sa.sa_mask);
    sa.sa_handler = sighand;
    sa.sa_flags = SA_RESTART;
    sigaction(SIGALRM, &sa, NULL);
    setitimer(ITIMER_REAL, &itv, NULL);
#endif
}

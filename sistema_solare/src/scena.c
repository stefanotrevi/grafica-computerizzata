#include "scena.h"
#include "control.h"
#include "loaders.h"
#include "planet.h"
#include "shapes/sphere.h"

#include <GL/freeglut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SAMP_NUM 4 // numero di campioni per gli FPS

#ifdef DOUBLE_BUFFER
    #define BUFFER_MODE GLUT_DOUBLE
#else
    #define BUFFER_MODE GLUT_SINGLE
#endif

static unsigned fps = 0U;

static GLuint bg_tex;
static sphere_t bg_sphere;

//static GLuint shader;
//static GLUquadric *bg_quad;

// clang-format off
// E' comodo usare la stessa struttura per il sole, anche se non usiamo tutti i campi
static planet_t sun = 
{
    .name = "Sole", 
    .specs = {.size = 700000 * SUN_SCALE, .rot_speed = 648},
    .mat = {.col = {1, .7, .3}}
};

static planet_t moon = 
{
    .name = "Luna",
    .tex = {[TEX_SURF] = 1},
    .specs = 
    {
        .dist = {.356 * MOON_SCALE, .406 * MOON_SCALE},
        .ecc = 0.055,
        .size = 1740,
        .rot_speed = 648
    },
    .mat = 
    {
        .shi = 11.264f,
        .col = {.7, .8, .9},
        .amb = {0.25f, 0.20725f, 0.20725f, 0.922f},
        .dif = {1.0f, 0.829f, 0.829f, 0.922f},
        .spe = {0.296648f, 0.296648f, 0.296648f, 0.922f}
    }
};

static planet_t planets[PLA_NUM] = 
{ 
    [PLA_MER] = 
    {
        .name = "Mercurio",
        .tex = {[TEX_SURF] = 1},
        .specs = 
        {
            .dist = {46, 70},
            .ecc = 0.2056,
            .size = 2440,
            .rot_speed = 1416
        },
        .mat = 
        {
            .shi = 25.6f,
            .col = {1, 1, 0},
            .amb = {0.2125f, 0.1275f, 0.054f, 1.0f},
            .dif = {0.714f, 0.4284f, 0.18144f, 1.0f},
            .spe = {0.393548f, 0.271906f, 0.166721f, 1.0f}
        }
    },
    [PLA_VEN] = 
    {
        .name = "Venere",
        .tex = {[TEX_SURF] = 1, [TEX_CLOUD] = 1},
        .specs = 
        {
            .dist = {107, 109},
            .ecc = 0.0068,
            .size = 6052,
            .rot_speed = -5832
        },
        .mat = 
        {
            .shi = 51.2f,
            .col = {1, 0, 0},
            .amb = {0.2295f, 0.08825f, 0.0275f, 1.0f},
            .dif = {0.5508f, 0.2118f, 0.066f, 1.0f}, 
            .spe = {0.580594f, 0.223257f, 0.0695701f, 1.0f}
        }
    },
    [PLA_TER] = 
    {
        .name = "Terra",
        .tex = {[TEX_SURF] = 1, [TEX_CLOUD] = 1, [TEX_NORM] = 1},
        .moon_num = 1,
        .moon = &moon,
        .specs = 
        {
            .dist = {147, 152},
            .ecc = 0.0554,
            .size = 6371,
            .rot_speed = 24
        },
        .mat = 
        {
            .shi = 12.8f,
            .col = {0, 1, 1},
            .amb = {0.1f, 0.18725f, 0.1745f, 0.8f},
            .dif = {0.396f, 0.74151f, 0.69102f, 0.8f},
            .spe = {0.297254f, 0.30829f, 0.306678f, 0.8f}
        }
    },
    [PLA_MAR] = 
    {
        .name = "Marte",
        .tex = {[TEX_SURF] = 1},
        .specs = 
        {
            .dist = {205, 249},
            .ecc = 0.0934,
            .size = 3390,
            .rot_speed = 25
        },
        .mat = 
        {
            .shi = 12.8f,
            .col = {.9, .5, .2},
            .amb = {0.19125f, 0.0735f, 0.0225f, 1.0f},
            .dif = {0.7038f, 0.27048f, 0.0828f, 1.0f},
            .spe = {0.256777f, 0.137622f, 0.086014f, 1.0f}
        }
    },
    [PLA_GIO] = 
    {
        .name = "Giove",
        .tex = {[TEX_SURF] = 1},
        .specs = 
        {
            .dist = {741, 817},
            .ecc = 0.0483,
            .size = 70000,
            .rot_speed = 10
        },
        .mat = 
        {
            .shi = 76.8f,
            .col = {.8, .3, .3},
            .amb = {0.25f, 0.148f, 0.06475f, 1.0f},
            .dif = {0.4f, 0.2368f, 0.1036f, 1.0f},
            .spe = {0.774597f, 0.458561f, 0.200621f, 1.0f}
        }
    },
    [PLA_SAT] = 
    {
        .name = "Saturno",
        .tex = {[TEX_SURF] = 1, [TEX_RING] = 1},
        .specs = 
        {
            .dist = {1350, 1510},
            .ecc = 0.0560,
            .size = 58200,
            .rot_speed = 11
        },
        .mat = 
        {
            .shi = 11.264f,
            .col = {.7, .8, .9},
            .amb = {0.25f, 0.20725f, 0.20725f, 0.922f},
            .dif = {1.0f, 0.829f, 0.829f, 0.922f},
            .spe = {0.296648f, 0.296648f, 0.296648f, 0.922f}
        }
    },
    [PLA_URA] = 
    {
        .name = "Urano",
        .tex = {[TEX_SURF] = 1},
        .specs = 
        {
            .dist = {2750, 3000},
            .ecc = 0.0461,
            .size = 25300,
            .rot_speed = -17
        },
        .mat = 
        {
            .shi = 32.0f,
            .col = {0, .7, .5},
            .amb = {0.0f, 0.1f, 0.06f, 1.0f},
            .dif = {0.0f, 0.50980392f, 0.50980392f, 1.0f},
            .spe = {0.50196078f, 0.50196078f, 0.50196078f, 1.0f}
        }
    },
    [PLA_NET] = 
    {
        .name = "Nettuno",
        .tex = {[TEX_SURF] = 1},
        .specs = 
        {
            .dist = {4450, 4550},
            .ecc = 0.0097,
            .size = 24600,
            .rot_speed = 16
        },
        .mat = 
        {
            .shi = 9.84615f,
            .col = {.3, .5, .7},
            .amb = {0.105882f, 0.058824f, 0.113725f, 1.0f},
            .dif = {0.427451f, 0.470588f, 0.541176f, 1.0f},
            .spe = {0.333333f, 0.333333f, 0.521569f, 1.0f}
        }
    }
};
// clang-format on

/*static void shadersInit(GLuint *shader)
{
    *shader = loadProgram("vert.vs", "frag.fs");

    loadUniform(*shader, "material.ambient", (GLfloat[]){.6f, .6f, .6f}, UNI_VEC3);
    loadUniform(*shader, "material.diffuse", (GLfloat[]){.6f, .6f, .6f}, UNI_VEC3);
    loadUniform(*shader, "material.specular", (GLfloat[]){.3f, .3f, .3f}, UNI_VEC3);
}
*/
static void lightingInit(void)
{
    GLfloat light_glb[] = {.1f, .1f, .1f, 1.f};
    GLfloat light_amb[] = {.7f, .7f, .7f, 1.f};
    GLfloat light_dif[] = {.7f, .7f, .7f, 1.f};
    GLfloat light_spc[] = {.3f, .3f, .3f, 1.f};

    // Proprietà dell'illuminazione e shader
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_glb);
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
    glShadeModel(GL_SMOOTH);

    // Imposto la sorgente di luce
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_amb);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_dif);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_spc);
}

static void frustumInit(void)
{
    GLfloat frust[] = {-.96, .96, -.54, .54, 1, SOLAR_SIZE};

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(frust[0], frust[1], frust[2], frust[3], frust[4], frust[5]);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

static void glInit(void)
{
    // Abilito profondità, illuminazione e blending
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glClearColor(0, 0, 0, 1);
}

static void sunInit(void)
{
    char fname[MAX_FLEN];

    sun.specs.rot_speed = (float)(TIME_SPEED * 360 / 24 / sun.specs.rot_speed);

    strcat(strcpy(fname, sun.name), ".jpg");
    sun.tex[TEX_SURF] = loadTexture(fname, GL_RGB);
    sun.surf = sphereInit(sun.specs.size, SPHERE_SECT, SPHERE_STACK, 1);
}

static void bgInit(void)
{
    bg_sphere = sphereInit(BG_SIZE, SPHERE_SECT, SPHERE_STACK, -1);
    bg_tex = loadTexture("stars_milky_way.jpg", GL_RGB);
}

static void planetsInit(void)
{
    for (enum solarpla i = PLA0; i < PLA_NUM; ++i)
        planetSetup(planets + i);
}

static void drawSun(void)
{
    glColor3fv(sun.mat.col);
    glBindTexture(GL_TEXTURE_2D, sun.tex[TEX_SURF]);

    glPushMatrix();
    {
        glRotatef(-90, 1, 0, 0);
        glRotatef(sun.specs.rot_pos, 0, 0, 1);
        sphereDraw(&sun.surf);
    }
    glPopMatrix();

    glRasterPos3f(0, sun.specs.size, sun.specs.size);
    drawBitmapString(GLUT_BITMAP_HELVETICA_12, sun.name);

    planetTic(&sun.specs);
}

static void drawBg(void)
{
    static const GLfloat amb_dif[] = {.3, .3, .35, 1};
    static const GLfloat spec[] = {.2, .2, .2, 1};
    static const GLfloat shi = .5;

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, amb_dif);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, spec);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shi);
    glBindTexture(GL_TEXTURE_2D, bg_tex);

    glPushMatrix();
    {
        glRotatef(-90, 1, 0, 0);
        sphereDraw(&bg_sphere);
    }
    glPopMatrix();
}

static void scena(void)
{
    static const GLfloat light_pos[] = {1, 1, 1, 1};

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

    drawBg();
    glPushMatrix();
    {
        glTranslatef(1, 1, 1);
        glDisable(GL_LIGHTING);
        drawSun();
        glEnable(GL_LIGHTING);

        for (int i = 0; i < PLA_NUM; ++i)
            planetDraw(planets + i);
    }
    glPopMatrix();

#ifdef DOUBLE_BUFFER
    glutSwapBuffers();
#else
    glFinish();
#endif
    glutPostRedisplay();
    ++fps;
}

void windowInit(void)
{
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
    glutInitDisplayMode(BUFFER_MODE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(512, 512);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Sistema Solare (FPS 0)");
}

void sceneInit(void)
{
    glInit();
    frustumInit();
    lightingInit();
    bgInit();
    sunInit();
    planetsInit();

    // Telecamera
    placeCamera();
    // Funzione di display
    glutDisplayFunc(scena);
}

void drawBitmapString(void *font, const char *str)
{
    while (*str)
        glutBitmapCharacter(font, *(str++));
}

void updateFPS(void)
{
    static char buf[64];
    static unsigned samp[SAMP_NUM] = {0}, old_fps = 0U;
    static int sec = 0;
    unsigned old_samp = samp[sec];

    samp[sec] = fps - old_fps;
    sec = (sec + 1) % SAMP_NUM;
    fps -= old_samp;
    old_fps = fps;

    snprintf(buf, sizeof buf, "Sistema Solare (%u FPS)", fps / SAMP_NUM);
    glutSetWindowTitle(buf);
}

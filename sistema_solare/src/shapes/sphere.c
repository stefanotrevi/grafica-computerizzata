#include "shapes/sphere.h"
#include "math/scalar.h"

#define __USE_XOPEN
#define _USE_MATH_DEFINES

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

static void sphereDataInit(GLfloat *data, float rad, int sect, int stack, float dir)
{
    float sectorStep = (float)(2 * M_PI / sect);
    float stackStep = (float)(M_PI / stack);
    float lengthInv = dir * 1.f / rad;

    for (int i = 0, k = 0; i <= stack; ++i)
    {
        float stackAngle = (float)(M_PI / 2 - i * stackStep); // starting from pi/2 to -pi/2
        float xy = rad * cosf(stackAngle);                    // r * cos(u)
        float z = rad * sinf(stackAngle);                     // r * sin(u)

        // add (sect_n+1) vertices per stack_n
        // the first and last vertices have same position and normal, but different tex coords
        for (int j = 0; j <= sect; ++j, k += 8)
        {
            float sectorAngle = j * sectorStep; // starting from 0 to 2pi
            float x = xy * cosf(sectorAngle);   // r * cos(u) * cos(v)
            float y = xy * sinf(sectorAngle);   // r * cos(u) * sin(v)

            // vertici
            data[k] = x;
            data[k + 1] = y;
            data[k + 2] = z;
            // normali
            data[k + 3] = x * lengthInv;
            data[k + 4] = y * lengthInv;
            data[k + 5] = z * lengthInv;
            // texture
            data[k + 6] = (float)j / sect;
            data[k + 7] = (float)i / stack;
        }
    }
}

static void sphereIdxInit(GLuint *idx, int sect, int stack)
{
    for (int i = 0, k = 0; i < stack; ++i)
    {
        int k1 = i * (sect + 1); // beginning of current stack_n
        int k2 = k1 + sect + 1;  // beginning of next stack_n

        for (int j = 0; j < sect; ++j, ++k1, ++k2)
        {
            // 2 triangles per sector excluding first and last stacks
            if (i == 0)
            {
                idx[k] = k1 + 1;
                idx[k + 1] = k2;
                idx[k + 2] = k2 + 1;
                k += 3;
            }
            else if (i == stack - 1)
            {
                idx[k] = k1;
                idx[k + 1] = k2;
                idx[k + 2] = k1 + 1;
                k += 3;
            }
            else
            {
                idx[k] = k1;
                idx[k + 1] = k2;
                idx[k + 2] = k1 + 1;
                idx[k + 3] = k1 + 1;
                idx[k + 4] = k2;
                idx[k + 5] = k2 + 1;
                k += 6;
            }
        }
    }
}

static void sphereToGPU(sphere_t *s)
{
    glGenBuffers(1, &s->data_vbo);
    glGenBuffers(1, &s->idx_vbo);

    glBindBuffer(GL_ARRAY_BUFFER, s->data_vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, s->idx_vbo);

    glBufferData(GL_ARRAY_BUFFER, s->data_sz * sizeof *s->data, s->data, GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, s->idx_sz * sizeof *s->idx, s->idx, GL_STATIC_DRAW);


    glInterleavedArrays(GL_T2F_N3F_V3F, SPHERE_BSTRIDE, NULL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

sphere_t sphereInit(float rad, int sect, int stack, float direction)
{
    sphere_t s = {
        .rad = rad,
        .sect = sect,
        .stack = stack,
        .data_sz = (stack + 1) * (sect + 1) * SPHERE_STRIDE,
        .idx_sz = (stack * sect - 1) * 6};

    s.data = malloc(s.data_sz * sizeof *s.data);
    s.idx = malloc(s.idx_sz * sizeof *s.idx);

    sphereDataInit(s.data, rad, sect, stack, direction);
    sphereIdxInit(s.idx, sect, stack);
    sphereToGPU(&s);

    sphereDelHost(&s);

    return s;
}

void sphereDelHost(sphere_t *s)
{
    free(s->data);
    free(s->idx);
}

void sphereDraw(sphere_t *s)
{
    glBindBuffer(GL_ARRAY_BUFFER, s->data_vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, s->idx_vbo);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glVertexPointer(3, GL_FLOAT, SPHERE_BSTRIDE, SPHERE_CORD_OFF);
    glNormalPointer(GL_FLOAT, SPHERE_BSTRIDE, SPHERE_NORM_OFF);
    glTexCoordPointer(2, GL_FLOAT, SPHERE_BSTRIDE, SPHERE_TEXT_OFF);

    glDrawElements(GL_TRIANGLES, s->idx_sz, GL_UNSIGNED_INT, NULL);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

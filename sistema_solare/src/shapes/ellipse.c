#include "shapes/ellipse.h"
#include "math/scalar.h"

#include <stdlib.h>
#define __USE_XOPEN
#include <math.h>

static void ellipseToGPU(ellipse_t *e)
{
    glGenBuffers(1, &e->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, e->vbo);
    glBufferData(GL_ARRAY_BUFFER, e->data_sz * sizeof *e->data, e->data, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

ellipse_t ellipseInit(float ax_min, float ax_maj, float off, int vert_n)
{
    float rad = 0, step = degToRad(360.f / vert_n);
    ellipse_t e = {
            .ax_min = ax_min,
            .ax_maj = ax_maj,
            .vert_n = vert_n,
            .data_sz = vert_n * 3};

    e.data = malloc(e.data_sz * sizeof *e.data);

    for (int i = 0; i < e.data_sz; i += 3, rad += step)
    {
        e.data[i] = ax_maj * sinf(rad);
        e.data[i + 1] = 0;
        e.data[i + 2] = ax_min * cosf(rad) + off;
    }
    ellipseToGPU(&e);
    ellipseFreeHost(&e);

    return e;
}

void ellipseFreeHost(ellipse_t *e)
{
    free(e->data);
}

void ellipseDraw(ellipse_t *e)
{
    glBindBuffer(GL_ARRAY_BUFFER, e->vbo);
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, NULL);
    
    glDrawArrays(GL_LINE_LOOP, 0, e->vert_n);
    
    glDisableClientState(GL_VERTEX_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

#include "shapes/ring.h"
#include "math/scalar.h"

#include <stdlib.h>
#define __USE_XOPEN
#include <math.h>

static void ringDataInit(GLfloat *data, GLsizei size, float rad_in, float rad_ext, int vert)
{
    float step = degToRad(360.f / vert);
    float inv_in = 1.f / rad_in;
    float inv_ext = 1.f / rad_ext;
    float rad = 0;

    for (int i = 0; i < size; i += 16, rad += step)
    {
        float c = cosf(rad), s = sinf(rad);
        float x1 = rad_in * c, y1 = rad_in * s;
        float x2 = rad_ext * c, y2 = rad_ext * s;

        // vertice 1
        data[i] = x1;
        data[i + 1] = y1;
        data[i + 2] = 0;
        // normali 1
        data[i + 3] = x1 * inv_in;
        data[i + 4] = y1 * inv_ext;
        data[i + 5] = 0;
        // texture 1
        data[i + 6] = 0;
        data[i + 7] = 0;

        // vertice 2
        data[i + 8] = x2;
        data[i + 9] = y2;
        data[i + 10] = 0;
        // normali 2
        data[i + 11] = x2 * inv_ext;
        data[i + 12] = y2 * inv_ext;
        data[i + 13] = 0;
        // texture 2
        data[i + 14] = 1;
        data[i + 15] = 1;
    }

    // chiudo l'anello
    for (int i = 0; i < 16; ++i)
        data[size + 15 - i] = data[i];
}

static void ringToGPU(ring_t *r)
{
    glGenBuffers(1, &r->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, r->vbo);
    glBufferData(GL_ARRAY_BUFFER, r->data_sz * sizeof *r->data, r->data, GL_STATIC_DRAW);
    glInterleavedArrays(GL_T2F_N3F_V3F, RING_STRIDE, NULL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

ring_t ringInit(float radius_in, float radius_ext, int vert_n)
{
    ring_t r = {
        .rad_in = radius_in,
        .rad_ext = radius_ext,
        .vert_n = vert_n * 2,
        .data_sz = vert_n * (2 * RING_CORD * RING_NORM * RING_TEXT)};

    r.data = malloc((r.data_sz + 16) * sizeof *r.data);

    ringDataInit(r.data, r.data_sz, radius_in, radius_ext, vert_n);
    ringToGPU(&r);
    ringDelHost(&r);

    return r;
}

void ringDelHost(ring_t *r)
{
    free(r->data);
}

void ringDraw(ring_t *ring)
{
    glBindBuffer(GL_ARRAY_BUFFER, ring->vbo);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glVertexPointer(3, GL_FLOAT, RING_STRIDE, RING_CORD_OFF);
    glNormalPointer(GL_FLOAT, RING_STRIDE, RING_NORM_OFF);
    glTexCoordPointer(2, GL_FLOAT, RING_STRIDE, RING_TEXT_OFF);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, ring->vert_n);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

#include "control.h"
#include "scena.h"
#include "signals.h"

#include <GL/glew.h>
#include <GL/freeglut.h>
#include <GL/gl.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef _WIN32
    #define CLEAR_SCREEN "cls"
#else
    #define CLEAR_SCREEN "clear"
#endif

int main(int argc, char **argv)
{
    if (system(CLEAR_SCREEN))
        perror("Errore a pulire il terminale");

    printf("Inizializzo glut... ");
    fflush(stdout);
    glutInit(&argc, argv);

    printf("OK!\nInizializzo la finestra... ");
    fflush(stdout);
    windowInit();

    printf("OK!\nInizializzo glew... ");
    fflush(stdout);
    glewInit();

    printf("OK!\nInizializzo la scena... ");
    fflush(stdout);
    sceneInit();

    printf("OK!\nInizializzo i segnali... ");
    fflush(stdout);
    sigInit();

    printf("OK!\nInizializzo i controlli... ");
    fflush(stdout);
    ctrlInit();

    printf("OK!\nTutto pronto!\n");
    printUsage();

    glutMainLoop();

    return 0;
}

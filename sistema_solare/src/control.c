#include "control.h"
#include "math/scalar.h"
#include "math/vec.h"
#include "res.h"

#include <GL/freeglut.h>
#include <stdio.h>

#ifdef _WIN32
    #include <Windows.h>
    #include <mmsystem.h>
#endif

#define MOUSE_SENS .1f     // sensibilità del mouse
#define KB_SLOW_DELTA 2.f  // velocità movimento tastiera
#define KB_FAST_DELTA 10.f // velocià "turbo"

#define ESC_KEY 27 // tasto escape

#define MUSIC_FILE RES_PREFIX "/sound.wav" // file canzone

static int mouse_x = 256, mouse_y = 256;

static camera_t cam = {
    .yaw = -272,
    .pitch = -44.9,
    .eye = {16, 440, -339},
    .front = {-.025, -.706, .708},
    .up = {0, 1, 0},
};

void printUsage(void)
{
    printf("--------LISTA COMANDI--------\n"
           "w: muovi avanti\n"
           "s: muovi indietro\n"
           "a: muovi a sinistra\n"
           "d: muovi a destra\n"
           "mouse (tieni premuto): ruota visuale\n"
           "m: ferma/fai partire musica\n"
           "ESC: esci\n"
           "h: stampa lista comandi\n"
           "t: regola velocità movimento (piano/veloce)\n"
           "f: schermo intero\n"
           "p: stampa info debug\n"
           "-----------------------------\n");
#ifndef DOUBLE_BUFFER
    printf("ATTENZIONE: lo schermo intero senza double buffer può causare artefatti!\n"
           "-----------------------------\n");
#endif
}

static void printDebug(void)
{
    printf("yaw: %f, pitch: %f\n"
           "eye: %f, %f, %f\n"
           "front: %f, %f, %f\n",
           cam.yaw, cam.pitch,
           cam.eye.x, cam.eye.y, cam.eye.z,
           cam.front.x, cam.front.y, cam.front.z);
}

void ctrlInit(void)
{
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutKeyboardFunc(keyboard);
}

void placeCamera(void)
{
    vec3_t ctr = sum(cam.eye, cam.front);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(cam.eye.x, cam.eye.y, cam.eye.z,
              ctr.x, ctr.y, ctr.z,
              cam.up.x, cam.up.y, cam.up.z);
}

void mouse(int btn, int state, int x, int y)
{
    if (btn == GLUT_LEFT && state == GLUT_DOWN)
    {
        mouse_x = x;
        mouse_y = y;
    }
}

void motion(int x, int y)
{
    float off_x = (x - mouse_x) * MOUSE_SENS;
    float off_y = (mouse_y - y) * MOUSE_SENS; // y è invertito

    mouse_x = x;
    mouse_y = y;

    cam.yaw += off_x;
    cam.pitch += off_y;

    if (cam.pitch > 89.f || cam.pitch < -89.f)
        cam.pitch -= off_y;

    cam.front = euler3(degToRad(cam.yaw), degToRad(cam.pitch));
    placeCamera();
}

void keyboard(unsigned char key, int x, int y)
{
    static int fs = 0;
#ifdef _WIN32
    static int music = 0;
#endif
    static float delta = KB_SLOW_DELTA;
    (void)x;
    (void)y;

    switch (key)
    {
    case 'w': // muovi in alto
    case 'W': cam.eye = sum(cam.eye, smul(cam.front, delta)); break;
    case 's': // muovi in basso
    case 'S': cam.eye = sub(cam.eye, smul(cam.front, delta)); break;
    case 'a': // muovi a sinistra
    case 'A': cam.eye = sub(cam.eye, smul(normalize(cross(cam.front, cam.up)), delta)); break;
    case 'd': // muovi a destra
    case 'D': cam.eye = sum(cam.eye, smul(normalize(cross(cam.front, cam.up)), delta)); break;
    case 'f': // vai a schermo intero
    case 'F': (fs = !fs) ? glutFullScreen() : glutReshapeWindow(512, 512); break;
    case 't': // usiamo > per silenziare l'avviso sulla comparazione FP
    case 'T': delta = (delta > KB_SLOW_DELTA) ? KB_SLOW_DELTA : KB_FAST_DELTA; return;
#ifdef _WIN32
    case 'm': // musica (solo su Windows)
    case 'M':
        (music = !music) ? PlaySound(MUSIC_FILE, NULL, SND_ASYNC | SND_LOOP) : PlaySound(NULL, 0, 0);
        return;
#endif
    case 'p':
    case 'P': printDebug(); return;
    case 'h':
    case 'H': printUsage(); return;
    case ESC_KEY: exit(EXIT_SUCCESS); return;
    default: return;
    }

    // riposiziono la telecamera
    placeCamera();
}

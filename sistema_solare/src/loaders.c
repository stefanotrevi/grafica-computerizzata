#include "loaders.h"
#include "res.h"
#include "stb_image.h"

#include <fcntl.h>
#include <string.h>

#ifdef _WIN32
    #include <Windows.h>
    #include <io.h>
#else
    #include <sys/mman.h>
    #include <unistd.h>
#endif

#define TEX_PREFIX RES_PREFIX "/2k/2k_"   // prefisso texture
#define HITEX_PREFIX RES_PREFIX "/8k/8k_" // prefisso texture HD
#define SHADER_PREFIX "shaders/"          // prefisso shader

static void loadAlphaJpeg(int w, int h, int chan, stbi_uc *data)
{
    int size = w * h * (chan + 1);
    stbi_uc *new_data = malloc(size * sizeof *new_data);

    for (int i = 0, j = 0; j < size; i += 3, j += 4)
    {
        new_data[j] = data[i];
        new_data[j + 1] = data[i + 1];
        new_data[j + 2] = data[i + 2];
        new_data[j + 3] = (stbi_uc)(data[i] | data[i + 1] | data[i + 2]);
    }
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, new_data);
    free(new_data);
}

static int file_exists(char *fname)
{
    int fd = open(fname, O_RDONLY);

    if (fd < 0)
        return 0;

    close(fd);

    return 1;
}

static void *portable_mmap(char *fname, size_t *size)
{
    void *map;

#ifdef _WIN32
    HANDLE f_map, fd = CreateFile(fname, GENERIC_READ, FILE_SHARE_READ, NULL,
                                  OPEN_EXISTING, FILE_ATTRIBUTE_READONLY, NULL);

    GetFileSize(fd, (unsigned long *)size);
    f_map = CreateFileMapping(fd, NULL, PAGE_READONLY, 0, 0, NULL);
    map = MapViewOfFile(f_map, FILE_MAP_READ, 0, 0, *size);

    CloseHandle(fd);
    CloseHandle(f_map);
#else
    int fd = open(fname, O_RDONLY);

    *size = lseek(fd, 0, SEEK_END);
    map = mmap(NULL, *size, PROT_READ, MAP_SHARED, fd, 0);
    close(fd);
#endif

    return map;
}

static void portable_munmap(void *map, size_t size)
{
#ifdef _WIN32
    (void)size;
    UnmapViewOfFile(map);
#else
    munmap(map, size);
#endif
}

GLuint loadTexture(char *fname, GLenum kind)
{
    int w, h, chan;
    GLuint id;
    stbi_uc *data;
    char real_name[MAX_FLEN];

#ifdef HIRES_TEX
    strcat(strcpy(real_name, HITEX_PREFIX), fname);
    if (!file_exists(real_name)) // fallback alle texture normali
        strcat(strcpy(real_name, TEX_PREFIX), fname);
#else
    strcat(strcpy(real_name, TEX_PREFIX), fname);
#endif

    data = stbi_load(real_name, &w, &h, &chan, 0);

    glGenTextures(1, &id);
    glBindTexture(GL_TEXTURE_2D, id);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    // I JPG non hanno una trasparenza, gliela diamo in base all'intensità del colore
    if (kind == GL_RGBA && !strncmp(strrchr(fname, '.'), ".jpg", 4))
        loadAlphaJpeg(w, h, chan, data);
    else
        glTexImage2D(GL_TEXTURE_2D, 0, kind, w, h, 0, kind, GL_UNSIGNED_BYTE, data);

    glGenerateMipmap(GL_TEXTURE_2D);
    stbi_image_free(data);

    return id;
}

GLuint loadShader(char *fname, GLenum type)
{
    GLuint shader = glCreateShader(type);
    char real_name[MAX_FLEN] = SHADER_PREFIX;
    size_t size;
    GLchar *fd_map = portable_mmap(strcat(real_name, fname), &size);

    glShaderSource(shader, 1, (const char *const *)&fd_map, (const GLint *)&size);
    glCompileShader(shader);

    portable_munmap(fd_map, size);

    return shader;
}

GLUquadric *loadQuadric(void)
{
    GLUquadric *quad = gluNewQuadric();

    gluQuadricDrawStyle(quad, GLU_FILL);
    gluQuadricTexture(quad, GL_TRUE);
    gluQuadricNormals(quad, GLU_SMOOTH);

    return quad;
}

GLuint loadProgram(char *f_vert, char *f_frag)
{
    GLuint prog = glCreateProgram();
    GLuint vert = loadShader(f_vert, GL_VERTEX_SHADER);
    GLuint frag = loadShader(f_frag, GL_FRAGMENT_SHADER);

    glAttachShader(prog, vert);
    glAttachShader(prog, frag);
    glLinkProgram(prog);

    // non servono più
    glDeleteShader(vert);
    glDeleteShader(frag);

    return prog;
}

void loadUniform(GLuint prog, const char *name, void *value, enum uniforms kind)
{
    GLint uni = glGetUniformLocation(prog, name);

    switch (kind)
    {
    case UNI_VEC3: glUniform3fv(uni, 1, value); return;
    case UNI_MAT4: glUniformMatrix4fv(uni, 1, GL_TRUE, value); return;
    default: return;
    }
}

#include "norm_cubemap.h"
#include "math/vec3.h"

#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>

int GenerateNormalisationCubeMap(void)
{
    unsigned char *data = malloc(32 * 32 * 3 * sizeof *data);
    if (!data)
    {
        fprintf(stderr, "Unable to allocate memory for texture data for cube map\n");
        return 0;
    }

    //some useful variables
    int size = 32;
    float offset = 0.5f;
    float halfSize = 16.0f;
    vec3_t tempVector;
    unsigned char *bytePtr;

    //positive x
    bytePtr = data;

    for (int j = 0; j < size; j++)
    {
        for (int i = 0; i < size; i++)
        {
            tempVector.x = halfSize;
            tempVector.y = -(j + offset - halfSize);
            tempVector.z = -(i + offset - halfSize);

            tempVector = normalize3(tempVector);
            tempVector = v3to01(tempVector);

            bytePtr[0] = (unsigned char)(tempVector.x * 255);
            bytePtr[1] = (unsigned char)(tempVector.y * 255);
            bytePtr[2] = (unsigned char)(tempVector.z * 255);

            bytePtr += 3;
        }
    }
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X_ARB,
                 0, GL_RGBA8, 32, 32, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

    //negative x
    bytePtr = data;

    for (int j = 0; j < size; j++)
    {
        for (int i = 0; i < size; i++)
        {
            tempVector.x = (-halfSize);
            tempVector.y = (-(j + offset - halfSize));
            tempVector.z = ((i + offset - halfSize));

            tempVector = normalize3(tempVector);
            tempVector = v3to01(tempVector);

            bytePtr[0] = (unsigned char)(tempVector.x * 255);
            bytePtr[1] = (unsigned char)(tempVector.y * 255);
            bytePtr[2] = (unsigned char)(tempVector.z * 255);

            bytePtr += 3;
        }
    }
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X_ARB,
                 0, GL_RGBA8, 32, 32, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

    //positive y
    bytePtr = data;

    for (int j = 0; j < size; j++)
    {
        for (int i = 0; i < size; i++)
        {
            tempVector.x = (i + offset - halfSize);
            tempVector.y = (halfSize);
            tempVector.z = ((j + offset - halfSize));

            tempVector = normalize3(tempVector);
            tempVector = v3to01(tempVector);

            bytePtr[0] = (unsigned char)(tempVector.x * 255);
            bytePtr[1] = (unsigned char)(tempVector.y * 255);
            bytePtr[2] = (unsigned char)(tempVector.z * 255);

            bytePtr += 3;
        }
    }
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y_ARB,
                 0, GL_RGBA8, 32, 32, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

    //negative y
    bytePtr = data;

    for (int j = 0; j < size; j++)
    {
        for (int i = 0; i < size; i++)
        {
            tempVector.x = (i + offset - halfSize);
            tempVector.y = (-halfSize);
            tempVector.z = (-(j + offset - halfSize));

            tempVector = normalize3(tempVector);
            tempVector = v3to01(tempVector);

            bytePtr[0] = (unsigned char)(tempVector.x * 255);
            bytePtr[1] = (unsigned char)(tempVector.y * 255);
            bytePtr[2] = (unsigned char)(tempVector.z * 255);

            bytePtr += 3;
        }
    }
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y_ARB,
                 0, GL_RGBA8, 32, 32, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

    //positive z
    bytePtr = data;

    for (int j = 0; j < size; j++)
    {
        for (int i = 0; i < size; i++)
        {
            tempVector.x = (i + offset - halfSize);
            tempVector.x = (-(j + offset - halfSize));
            tempVector.x = (halfSize);

            tempVector = normalize3(tempVector);
            tempVector = v3to01(tempVector);

            bytePtr[0] = (unsigned char)(tempVector.x * 255);
            bytePtr[1] = (unsigned char)(tempVector.y * 255);
            bytePtr[2] = (unsigned char)(tempVector.z * 255);

            bytePtr += 3;
        }
    }
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z_ARB,
                 0, GL_RGBA8, 32, 32, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

    //negative z
    bytePtr = data;

    for (int j = 0; j < size; j++)
    {
        for (int i = 0; i < size; i++)
        {
            tempVector.x = (-(i + offset - halfSize));
            tempVector.x = (-(j + offset - halfSize));
            tempVector.x = (-halfSize);

            tempVector = normalize3(tempVector);
            tempVector = v3to01(tempVector);

            bytePtr[0] = (unsigned char)(tempVector.x * 255);
            bytePtr[1] = (unsigned char)(tempVector.y * 255);
            bytePtr[2] = (unsigned char)(tempVector.z * 255);

            bytePtr += 3;
        }
    }
    glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z_ARB,
                 0, GL_RGBA8, 32, 32, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

    free(data);

    return 1;
}

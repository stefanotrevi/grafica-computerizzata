#include "math/vec3.h"
#include "math/scalar.h"

#define __USE_XOPEN
#define _USE_MATH_DEFINES

#include <math.h>

vec3_t sum3(vec3_t v1, vec3_t v2)
{
    return (vec3_t){v1.x + v2.x, v1.y + v2.y, v1.z + v2.z};
}

vec3_t sub3(vec3_t v1, vec3_t v2)
{
    return (vec3_t){v1.x - v2.x, v1.y - v2.y, v1.z - v2.z};
}

vec3_t mul3(vec3_t v1, vec3_t v2)
{
    return (vec3_t){v1.x * v2.x, v1.y * v2.y, v1.z * v2.z};
}

vec3_t cross3(vec3_t v1, vec3_t v2)
{
    return (vec3_t){v1.y * v2.z - v1.z * v2.y,
                    v1.z * v2.x - v1.x * v2.z,
                    v1.x * v2.y - v1.y * v2.x};
}

vec3_t smul3(vec3_t v, float s)
{
    return (vec3_t){v.x * s, v.y * s, v.z * s};
}

float norm3(vec3_t v)
{
    return sqrtf(v.x * v.x + v.y * v.y + v.z * v.z);
}

vec3_t normalize3(vec3_t v)
{
    return smul3(v, 1.f / norm3(v));
}

vec3_t euler3(float yaw, float pitch)
{
    float cosp = cosf(pitch);

    return (vec3_t){cosf(yaw) * cosp, sinf(pitch), sinf(yaw) * cosp};
}

vec3_t v3to01(vec3_t v)
{
    return sum3(smul3(normalize3(v), .5f), (vec3_t){.5f, .5f, .5f});
}

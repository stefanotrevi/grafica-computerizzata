#include "math/vec2.h"
#include "math/scalar.h"

#define __USE_XOPEN
#define _USE_MATH_DEFINES

#include <math.h>

vec2_t sum2(vec2_t v1, vec2_t v2)
{
    return (vec2_t){v1.x + v2.x, v1.y + v2.y};
}

vec2_t sub2(vec2_t v1, vec2_t v2)
{
    return (vec2_t){v1.x - v2.x, v1.y - v2.y};
}

vec2_t mul2(vec2_t v1, vec2_t v2)
{
    return (vec2_t){v1.x * v2.x, v1.y * v2.y};
}

vec2_t cross2(vec2_t v1, vec2_t v2)
{
    (void)v1;
    (void)v2;

    return (vec2_t){0};
}

vec2_t smul2(vec2_t v, float s)
{
    return (vec2_t){v.x * s, v.y * s};
}

float norm2(vec2_t v)
{
    return sqrtf(v.x * v.x + v.y * v.y);
}

vec2_t normalize2(vec2_t v)
{
    return smul2(v, (float)(1. / norm2(v)));
}

vec2_t euler2(float yaw, float pitch)
{
    return (vec2_t){cosf(yaw) * cosf(pitch), sinf(pitch)};
}

vec2_t v2to01(vec2_t v)
{
    return sum2(smul2(normalize2(v), .5f), (vec2_t){.5f, .5f});
}

#define __USE_MISC
#define __USE_XOPEN
#define _USE_MATH_DEFINES

#include "math/scalar.h"
#include <math.h>
#include <stdlib.h>

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795f
#endif

float degToRad(float deg)
{
    return (float)(deg * (M_PI / 180.));
}

float radToDeg(float rad)
{
    return (float)(rad * (180. / M_PI));
}

void incmax(float *f, float delta, float max)
{
    if ((*f += delta) > max)
        *f = 0;
}

float frand(float min, float max)
{
    return (float)rand() / RAND_MAX * max + min;
}

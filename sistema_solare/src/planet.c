#include "planet.h"
#include "shapes/sphere.h"
#include "shapes/ring.h"
#include "shapes/ellipse.h"
#include "loaders.h"
#include "math/scalar.h"
#include "norm_cubemap.h"
#include "scena.h"

#define __USE_XOPEN
#define _USE_MATH_DEFINES

#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <math.h>
#include <string.h>
#include <GL/freeglut.h>

static void planetSetupSpecs(struct planet_spec *specs)
{
    // scalo la dimensione e le distanze dal sole
    specs->size *= PLANET_SCALE;
    specs->dist[0] *= SOLAR_SCALE;
    specs->dist[1] *= SOLAR_SCALE;

    // terza legge di Keplero (non uso la seconda, quindi la velocità è costante)
    specs->speed = (float)(sqrt(1. / CUBE(specs->dist[0])) * TIME_SPEED);

    // calcolo i semiassi e la distanza focale
    specs->axes[1] = specs->dist[0] / (1 - specs->ecc);
    specs->off = specs->axes[1] * specs->ecc;
    specs->axes[0] = specs->dist[1] - specs->off;

    // velocità di rotazione
    specs->rot_speed = (float)(TIME_SPEED * 360 / 24 / specs->rot_speed);
}

void planetSetup(planet_t *pla)
{
    char fname[MAX_FLEN];

    planetSetupSpecs(&pla->specs);
    pla->orbit = ellipseInit(pla->specs.axes[0], pla->specs.axes[1],
                             pla->specs.off, ELLIPSE_VERTEX);

    // Superfice
    if (pla->tex[TEX_SURF])
    {
        strcat(strcpy(fname, pla->name), ".jpg");
        pla->tex[TEX_SURF] = loadTexture(fname, GL_RGB);
        pla->surf = sphereInit(pla->specs.size, SPHERE_SECT, SPHERE_STACK, 1);
    }
    // Nuvole
    if (pla->tex[TEX_CLOUD])
    {
        strcat(strcpy(fname, pla->name), "_cloud.jpg");
        pla->tex[TEX_CLOUD] = loadTexture(fname, GL_RGBA);
        pla->clouds = sphereInit(pla->specs.size + CLOUD_HEIGHT, SPHERE_SECT, SPHERE_STACK, 1);
        pla->specs.cloud_speed = pla->specs.rot_speed * CLOUD_SPEED;
    }
    // Anelli
    if (pla->tex[TEX_RING])
    {
        strcat(strcpy(fname, pla->name), "_ring.png");
        pla->tex[TEX_RING] = loadTexture(fname, GL_RGBA);
        pla->ring = ringInit(pla->specs.size + RING_IN_DIST,
                             pla->specs.size + RING_EX_DIST, RING_SEGMENT);
    }
    // Normale
    if (pla->tex[TEX_NORM])
    {
        strcat(strcpy(fname, pla->name), "_norm.tif");
        pla->tex[TEX_NORM] = loadTexture(fname, GL_RGB);
        glGenTextures(1, pla->tex + TEX_SPEC);
        glBindTexture(GL_TEXTURE_CUBE_MAP_ARB, pla->tex[TEX_SPEC]);
        // non funziona
        /*GenerateNormalisationCubeMap();
        glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_CUBE_MAP_ARB, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);*/
    }

    // Lune: sono pianeti ricorsivi, anch'esse già "allocate"
    for (int i = 0; i < pla->moon_num; ++i)
    {
        planetSetup(pla->moon + i);
        pla->moon[i].specs.speed *= MOON_SPEED_SCALE; // altrimenti va troppo veloce
    }
}

void planetTic(struct planet_spec *spec)
{
    incmax(&spec->pos, spec->speed, 2 * M_PI);
    incmax(&spec->rot_pos, spec->rot_speed, 360);
    incmax(&spec->cloud_pos, spec->cloud_speed, 360);
}

static void setMaterial(const struct material *mat)
{
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat->amb);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat->dif);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat->spe);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, mat->shi);
    glColor3fv(mat->col);
}

// Non funziona
/*
static void planetBumpDraw(planet_t *pla)
{
    glBindBuffer(GL_ARRAY_BUFFER, pla->surf.data_vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pla->surf.idx_vbo);

    // carico il bump map
    glBindTexture(GL_TEXTURE_2D, pla->tex[TEX_NORM]);

    // carico il cube map
    glActiveTextureARB(GL_TEXTURE1_ARB);
    glBindTexture(GL_TEXTURE_CUBE_MAP_ARB, pla->tex[TEX_SPEC]);
    glEnable(GL_TEXTURE_CUBE_MAP_ARB);
    glActiveTextureARB(GL_TEXTURE0_ARB);

    // elementi del bump map
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glVertexPointer(3, GL_FLOAT, SPHERE_BSTRIDE, SPHERE_CORD_OFF);
    glTexCoordPointer(2, GL_FLOAT, SPHERE_BSTRIDE, SPHERE_TEXT_OFF);

    // elementi del cube map
    glClientActiveTextureARB(GL_TEXTURE1_ARB);
    glTexCoordPointer(2, GL_FLOAT, SPHERE_BSTRIDE, SPHERE_NORM_OFF);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    // eseguiamo la trasformazione
    glClientActiveTextureARB(GL_TEXTURE0_ARB);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_ARB);
    glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB_ARB, GL_TEXTURE);
    glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB_ARB, GL_REPLACE);
    glActiveTextureARB(GL_TEXTURE1_ARB);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_ARB);
    glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE0_RGB_ARB, GL_TEXTURE);
    glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB_ARB, GL_DOT3_RGB_ARB);
    glTexEnvi(GL_TEXTURE_ENV, GL_SOURCE1_RGB_ARB, GL_PREVIOUS_ARB);

    // disegnamo il bump map
    glActiveTextureARB(GL_TEXTURE0_ARB);
    glDrawElements(GL_TRIANGLES, pla->surf.idx_sz, GL_UNSIGNED_INT, NULL);

    // disabilitiamo tutto
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glActiveTextureARB(GL_TEXTURE1_ARB);
    glClientActiveTextureARB(GL_TEXTURE1_ARB);
    glDisable(GL_TEXTURE_CUBE_MAP_ARB);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glActiveTexture(GL_TEXTURE0);
    glClientActiveTextureARB(GL_TEXTURE0_ARB);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    glBlendFunc(GL_DST_COLOR, GL_ZERO);

    sphereDraw(&pla->surf);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
}
*/

void planetDraw(planet_t *pla)
{
    setMaterial(&pla->mat);

    glPushMatrix();
    {
        GLfloat tras_x = pla->specs.axes[1] * sinf(pla->specs.pos);
        GLfloat tras_z = pla->specs.axes[0] * cosf(pla->specs.pos) + pla->specs.off;

        glTranslatef(tras_x, 0, tras_z);
        // Disegno le lune
        for (int i = 0; i < pla->moon_num; ++i)
            planetDraw(pla->moon + i);

        // raddrizzo l'asse
        glRotatef(-90, 1, 0, 0);
        if (pla->tex[TEX_SURF])
        {
            // imposto l'asse di rotazione
            glPushMatrix();
            {
                glRotatef(pla->specs.rot_pos, 0, 0, 1);

                glBindTexture(GL_TEXTURE_2D, pla->tex[TEX_SURF]);
                sphereDraw(&pla->surf);
            }
            glPopMatrix();
        }
        if (pla->tex[TEX_CLOUD])
        {
            glPushMatrix();
            {
                glRotatef(pla->specs.cloud_pos, 1, 0, 0);
                glBindTexture(GL_TEXTURE_2D, pla->tex[TEX_CLOUD]);
                sphereDraw(&pla->clouds);
            }
            glPopMatrix();
        }
        if (pla->tex[TEX_RING])
        {
            glPushMatrix();
            {
                glRotatef(15, 0, 1, 0);
                glBindTexture(GL_TEXTURE_2D, pla->tex[TEX_RING]);
                ringDraw(&pla->ring);
            }
            glPopMatrix();
        }

        // Etichetta
        glBindTexture(GL_TEXTURE_2D, 0);
        glDisable(GL_LIGHTING);
        glRasterPos3f(0, pla->specs.size, pla->specs.size);
        drawBitmapString(GLUT_BITMAP_HELVETICA_12, pla->name);
    }
    glPopMatrix();

    glColor4f(pla->mat.col[0], pla->mat.col[1], pla->mat.col[2], 0.25);
    ellipseDraw(&pla->orbit);

    glEnable(GL_LIGHTING);

    planetTic(&pla->specs);
}

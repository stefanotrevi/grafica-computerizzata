#include <GL/glew.h>
#include <GL/freeglut.h>
#include <ft2build.h>
#include <fcntl.h>
#include <sys/mman.h>

#include FT_FREETYPE_H

#include "../include/vec3.h"

#define TTFMAP_SZ 128      // dimensione mappa caratteri

typedef struct ttfchar
{
    unsigned id;
    unsigned width;
    unsigned height;
    unsigned bear_l;
    unsigned bear_t;
    unsigned adv;
} ttfchar_t;

static GLuint VAO, VBO, text_shader;
static ttfchar_t ttfchar_map[TTFMAP_SZ];

static void initFt(void)
{
    FT_Library ft;
    FT_Face face;

    FT_Init_FreeType(&ft);
    FT_New_Face(ft, "/usr/share/fonts/X11/TTF/ClearSans-Regular.ttf", 0, &face);
    FT_Set_Pixel_Sizes(face, 0, 48);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    for (FT_ULong c = 0; c < TTFMAP_SZ; ++c)
    {
        GLuint text;
        FT_GlyphSlot glyph;
        FT_Bitmap bm;

        FT_Load_Char(face, c, FT_LOAD_RENDER);
        glyph = face->glyph;
        bm = glyph->bitmap;
        glGenTextures(1, &text);
        glBindTexture(GL_TEXTURE_2D, text);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, bm.width, bm.rows, 0, GL_RED, GL_UNSIGNED_BYTE, bm.buffer);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        ttfchar_map[c] = (ttfchar_t){.id = text,
                                     .width = bm.width,
                                     .height = bm.rows,
                                     .bear_l = glyph->bitmap_left,
                                     glyph->bitmap_top,
                                     glyph->advance.x};
    }
    glBindTexture(GL_TEXTURE_2D, 0);
    FT_Done_Face(face);
    FT_Done_FreeType(ft);
}

GLuint loadShader(char *fname, int type)
{
    GLchar *fd_map;
    int fd = open(fname, O_RDONLY);
    GLuint shader = glCreateShader(type);
    GLint size = lseek(fd, 0, SEEK_END);

    fd_map = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
    close(fd);
    glShaderSource(shader, 1, &fd_map, &size);
    glCompileShader(shader);
    munmap(fd_map, size);

    return shader;
}

void glPuts(GLuint shader, char *str, vec3_t pos, GLfloat scale, vec3_t col)
{
    // Carica lo shader
    glUseProgram(shader);
    glUniform3f(glGetUniformLocation(shader, "txt_color"), col.x, col.y, col.z);
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(VAO);

    // Stampa i caratteri
    for (; *str; ++str)
    {
        ttfchar_t c = ttfchar_map[*str];
        GLfloat x = pos.x + c.bear_l * scale;
        GLfloat y = pos.y - (c.height - c.bear_t) * scale;
        GLfloat w = c.width * scale;
        GLfloat h = c.height * scale;
        GLfloat vert[6][5] = {
                {x, y + h, 0, 0, 0},
                {x + w, y, 0, 1, 1},
                {x, y, 0, 0, 1},
                {x, y + h, 0, 0, 0},
                {x + w, y + h, 0, 1, 0},
                {x + w, y, 0, 1, 1},
        };
        glBindTexture(GL_TEXTURE_2D, c.id);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vert), vert);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        pos.x += (c.adv >> 6) * scale; // shifto di 6 per ottenere il valore in pixel
    }
    glBindVertexArray(0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

static void initTxtShader(GLfloat *frust)
{
    GLuint vert = loadShader("text.vert", GL_VERTEX_SHADER);
    GLuint frag = loadShader("text.frag", GL_FRAGMENT_SHADER);

    text_shader = glCreateProgram();
    glAttachShader(text_shader, vert);
    glAttachShader(text_shader, frag);
    glLinkProgram(text_shader);
    glDeleteShader(vert);
    glDeleteShader(frag);
    glUseProgram(text_shader);
    glUniformMatrix4fv(glGetUniformLocation(text_shader, "projection"), 1, GL_FALSE, frust);

    // Inizializzo FreeType
    initFt();

    // Genero i VBO per il testo
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    // Servono 6 vertici per un quadrato, e 4 float per ogni vertice
    glBufferData(GL_ARRAY_BUFFER, 6 * 4 * sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), NULL);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

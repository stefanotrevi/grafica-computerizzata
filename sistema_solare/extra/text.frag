#version 330 core

in vec2 tex_coord;
out vec4 color;

uniform sampler2D txt;
uniform vec3 txt_color;

void main()
{
    color = vec4(txt_color, 1) * vec4(1, 1, 1, texture(txt, tex_coord).r);
}

############ VARIABILI DI ESECUZIONE ############

# velocità di movimento degli oggetti
TIME_SPEED := 0.25
# attiva/disattiva il double buffer (e.g. per un benchmark) 
DOUBLE_BUFFER := 1
# attiva/disattiva le texture in alta risoluzione (solo sfondo e Terra)
HIRES_TEX := 1

############ VARIABILI DI COMPILAZIONE ############
# abilita la configurazione di debug 
DEBUG := 0
# usa il compilatore Intel (variabili $IC e $ICFLAGS), oppure $CC e $CFLAGS (anche su Windows!)
INTEL_COMPILER := 1
# abilita IPO
INTEL_IPO := 1
############ FINE VARIABILI UTENTE ############


# valori di default in caso non siano impostati di sistema
ifeq ($(CC),)
    CC := cc
    CFLAGS := -O2 -Wall
endif

ifeq ($(INTEL_COMPILER), 1)
    CC := $(IC)
    CFLAGS := $(ICFLAGS)
    ifeq ($(INTEL_IPO), 1)
        ifeq ($(OS), Windows_NT)
            CFLAGS += -Qipo
        else
            CFLAGS += -ipo
        endif
    endif
endif

ifeq ($(DEBUG), 1)
    CFLAGS := -g
endif

# cartelle e file di input/output
BINDIR := bin
OBJDIR := build

SRCDIR := src
SRC := $(wildcard $(SRCDIR)/*.c $(SRCDIR)/*/*.c)
SRCNAMES := $(notdir $(SRC))

INCDIR := include
INC := $(wildcard $(INCDIR)/*.h $(INCDIR)/*/*.h)
INCNAMES := $(notdir $(INC))

# Flag aggiuntive
CFLAGS += -I$(INCDIR) -DTIME_SPEED=$(TIME_SPEED) -D_POSIX_C_SOURCE=200809L

ifeq ($(DOUBLE_BUFFER), 1)
    CFLAGS += -DDOUBLE_BUFFER
endif
ifeq ($(HIRES_TEX), 1)
    CFLAGS += -DHIRES_TEX
endif

# opzioni del linker, dipendono dal SO
ifeq ($(OS), Windows_NT)
    CFLAGS += -Fo:$(OBJDIR)/ -Qdiag-disable=13000,13046,1786
    LDFLAGS := -link glew32.lib opengl32.lib GLU32.lib
    OBJEXT := obj
else
    LDFLAGS := -lGL -lglut -lm -lGLU -lGLEW -s
    OBJEXT := o
endif

OBJ := $(SRCNAMES:%.c=$(OBJDIR)/%.$(OBJEXT))

# ricetta base
all: folders solar_system

folders:
	mkdir -p $(BINDIR) $(OBJDIR)

# compilazione IPO
ifeq ($(INTEL_IPO), 1)
solar_system: $(SRC) $(INC)
	$(CC) $(CFLAGS) -o $(BINDIR)/$@ $(SRC) $(LDFLAGS)
else
# compilazione standard
solar_system: $(OBJ)
	$(CC) $(CFLAGS) -o $(BINDIR)/$@ $^ $(LDFLAGS)

$(OBJDIR)/%.$(OBJEXT): $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c -o $@ $^

$(OBJDIR)/%.$(OBJEXT): $(SRCDIR)/*/%.c
	$(CC) $(CFLAGS) -c -o $@ $^
endif

clean:
	rm -f build/* bin/*

# solo per Linux
install:
	mv bin/solar_system  /usr/local/bin

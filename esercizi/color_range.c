#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif

#include <GL/glut.h>
#include <signal.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

static int fps = 0;
static GLfloat red = 0, green = 0, blue = 0, step = 0.01;
static timer_t timer;

static void sighand(int sig)
{
    static char buf[12];

    if (sig != SIGUSR1)
        return;

    sprintf(buf, "%d", fps);
    fps = 0;
    glutSetWindowTitle(buf);
}

static void setColors(void)
{
    if (step > 0)
    {
        if (blue >= 1)
            blue += (step = -step);
        else if (green >= 1)
            blue += step;
        else if (red >= 1)
            green += step;
        else
            red += step;
    }
    else
    {
        if (red < 0)
            red += (step = -step);
        else if (green < 0)
            red += step;
        else if (blue < 0)
            green += step;
        else
            blue += step;
    }
}

static void display(void)
{
    setColors();
    glClearColor(red, green, blue, 0);
    glClear(GL_COLOR_BUFFER_BIT);
    // ho finito di disegnare
    glFinish();
    // swap dei buffer per il V-sync
    glutSwapBuffers();
    // Forzo la stampa
    glutPostRedisplay();
    ++fps;
}

int main(int argc, char **argv)
{
    struct sigevent se = {.sigev_notify = SIGEV_SIGNAL, .sigev_signo = SIGUSR1};
    struct itimerspec its = {{.tv_sec = 1}, {.tv_sec = 1}};
    struct sigaction sa = {0};

    sigemptyset(&sa.sa_mask);
    sa.sa_handler = sighand;
    sa.sa_flags = SA_RESTART;
    sigaction(SIGUSR1, &sa, NULL);

    timer_create(CLOCK_REALTIME, &se, &timer);
    timer_settime(timer, 0, &its, NULL);

    // inizializza glut
    glutInit(&argc, argv);
    // imposta tipo, dimensioni e posizione finestra
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(640, 480);
    // crea la finestra
    glutCreateWindow("Finestra");
    glClearColor(1, 1, 1, 0);

    glutDisplayFunc(display);
    glutMainLoop();
    timer_delete(timer);

    return 0;
}

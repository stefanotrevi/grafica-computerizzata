#define _POSIX_C_SOURCE 200809L
#include <GL/glew.h>
#include <GL/glut.h>
#include <signal.h>
#include <stdio.h>
#include <sys/time.h>
#include <math.h>

#define LIGHT_AMB
#define LIGHT_DIF
#define LIGHT_SPC

#define REF_RATE 144
#define LIGHT_DELTA (4.f / REF_RATE)
#define LIGHT_DIST 5.f

static int fps = 0;

static GLfloat light_x = .0f, light_y = .0f, light_z = .0f;

static void sighand(int sig)
{
    char title[12];

    sprintf(title, "%d", fps);
    glutSetWindowTitle(title);
    fps = 0;
    (void)sig;
}

static void display(void)
{
    GLfloat light_pos[] = {LIGHT_DIST * cosf(light_x), LIGHT_DIST * sinf(light_x), light_z, 1.f};
    GLfloat *mat_amb = light_x > 0 ? (GLfloat[]){.05f, .0f, .0f, 1.f} : (GLfloat[]){.0f, .05f, .05f, 1.f};
    GLfloat *mat_dif = light_x > 0 ? (GLfloat[]){.5f, .4f, .4f, 1.f} : (GLfloat[]){.4f, .5f, .5f, 1.f};
    GLfloat *mat_spe = light_x > 0 ? (GLfloat[]){.7f, .04f, .04f, 1.f} : (GLfloat[]){.04f, .7f, .7f, 1.f};

    glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix();

    glTranslatef(0.f, 0.f, -3.f);
    glRotatef(45.f, 1.f, 0.f, 0.f);

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat_amb);
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_dif);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_spe);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, .5f);
    glEnable(GL_LIGHT0);

    glutSolidSphere(1., 256, 256);
    glPopMatrix();
    glFinish();
    ++fps;
    // sarebbe 2pi, ma non importa che sia preciso, basta che sia maggiore
    if ((light_x += LIGHT_DELTA) > 6.3f)
        light_x = -6.3f;

    glutSwapBuffers();
    glutPostRedisplay();
    //glutTimerFunc(1000 / REF_RATE, (void (*)(int))glutPostRedisplay, 0);
}

static void keyboard(unsigned char key, int x, int y)
{
    switch (key)
    {
    case 27: exit(EXIT_SUCCESS); break;
    case 'w': light_y += LIGHT_DELTA; break;
    case 'a': light_x -= LIGHT_DELTA; break;
    case 's': light_y -= LIGHT_DELTA; break;
    case 'd': light_x += LIGHT_DELTA; break;
    default: break;
    }
    (void)x;
    (void)y;
}

static void init(void)
{
    GLfloat light_glb[] = {.0f, .0f, .0f, 1.f};
    GLfloat light_amb[] = {.5f, .5f, .5f, 1.f};
    GLfloat light_dif[] = {.5f, .5f, .5f, 1.f};
    GLfloat light_spc[] = {.4f, .4f, .4f, 1.f};
    glClearColor(1.f, 1.f, 1.f, 0.f);

    // Abilito la terza dimensione in visuale prospettica
    glEnable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glEnable(GL_LIGHTING);
    glLoadIdentity();
    glFrustum(-1.0, 1.0, -1.0, 1.0, 1, 10.0);
    // bordo (GL_LINE) o pieno (GL_FILL)
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    // abilito l'illuminazione
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_amb);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_dif);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_spc);

    // Illuminazione globale
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_glb);
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
    glShadeModel(GL_SMOOTH);
}

int main(int argc, char **argv)
{
    struct itimerval itv = {{.tv_sec = 1}, {.tv_sec = 1}};
    struct sigaction sa = {0};

    // setup finestra
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(0, 0);

    // setup gestore segnali e timer
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = sighand;
    sa.sa_flags = SA_RESTART;
    sigaction(SIGALRM, &sa, NULL);
    setitimer(ITIMER_REAL, &itv, NULL);

    // titolo e sfondo
    glutCreateWindow("0");
    glewInit();
    init();
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutMainLoop();

    return 0;
}

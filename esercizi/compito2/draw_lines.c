#define _POSIX_C_SOURCE 200809L

#include <GL/glut.h>
#include <signal.h>
#include <stdio.h>
#include <sys/time.h>

#define START_OFF 8
#define MAX_LINES 4
#define MAX_SEC 2
#define GAP_X (.75f / MAX_LINES)

static int line_shift = 0, display_results = 0, elap = 0;
static int fps[MAX_LINES][MAX_SEC] = {0};
static float graph_step;

static void sighand(int sig)
{
    if (++elap < MAX_SEC)
        return;

    elap = 0;

    if (++line_shift < MAX_LINES)
        return;

    int fps_max = 0;
    const struct itimerval itv = {0};
    char title[64], *p_t = title;

    // trovo le medie e i massimi
    for (int i = 0; i < line_shift; ++i)
    {
        for (int j = 1; j < MAX_SEC; ++j)
            fps[i][0] += fps[i][j];

        if ((fps[i][0] /= MAX_SEC) > fps_max)
            fps_max = fps[i][0];

        p_t += i ? sprintf(p_t, ", %d", fps[i][0]) : sprintf(p_t, "%d", fps[i][0]);
    }
    glutSetWindowTitle(title);
    graph_step = .75f / fps_max;
    display_results = 1;
    // resetto il segnale, non serve più
    setitimer(ITIMER_REAL, &itv, NULL);
    signal(sig, SIG_IGN);
}

static float randf(void)
{
    return (float)(rand() % 101) / 100.f;
}

static void display(void)
{
    if (display_results)
    {
        glClearColor(1.0f, 1.0f, 1.0f, 0.f);
        glClear(GL_COLOR_BUFFER_BIT);
        glBegin(GL_LINES);
        glColor3f(0.f, 0.f, 0.f);
        for (int i = 0; i < line_shift; ++i)
        {
            float pos_x = GAP_X * i;

            glVertex2f(pos_x, 0.f);
            glVertex2f(pos_x, graph_step * fps[i][0]);
        }

        glEnd();
        glFinish();
        if (display_results == 1)
        {
            ++display_results;
            glutPostRedisplay();
        }
    }
    else
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glBegin(GL_LINES);
        glColor3f(randf(), randf(), randf());
        for (int i = 0, max = 1 << (line_shift + START_OFF); i < max; ++i)
        {
            glVertex2f(0.f, 0.f);
            glVertex2f(randf(), randf());
        }
        glEnd();

        glFinish();
        glutPostRedisplay();
        ++fps[line_shift][elap];
    }
}

int main(int argc, char **argv)
{
    struct itimerval itv = {{.tv_sec = 1}, {.tv_sec = 1}};
    struct sigaction sa = {0};

    // setup finestra
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(0, 0);

    // setup gestore segnali e timer
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = sighand;
    sa.sa_flags = SA_RESTART;
    sigaction(SIGALRM, &sa, NULL);
    setitimer(ITIMER_REAL, &itv, NULL);

    // avvio UI
    glutCreateWindow("0");
    glClearColor(1.f, 1.f, 1.f, 0.f);
    glutDisplayFunc(display);
    glutMainLoop();

    return 0;
}

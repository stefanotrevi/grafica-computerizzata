#define _POSIX_C_SOURCE 200809L
#include <GL/glew.h>
#include <GL/glut.h>
#include <signal.h>
#include <stdio.h>
#include <sys/time.h>

#define NFACE 4
#define NSTRIP 4
#define NVERTEX 4

static int fps = 0;

static void sighand(int sig)
{
    char title[12];

    sprintf(title, "%d", fps);
    glutSetWindowTitle(title);
    fps = 0;
    (void)sig;
}

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix();

    glColor3ub(255, 0, 0);
    glutWireTeapot(.5);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(.0f, -.5f, .0f);
    glColor3ub(255, 0, 255);
    glutSolidCube(.25);
    glPopMatrix();

    glFinish();
    //    glutPostRedisplay();
    ++fps;
}

int main(int argc, char **argv)
{
    struct itimerval itv = {{.tv_sec = 1}, {.tv_sec = 1}};
    struct sigaction sa = {0};

    // setup finestra
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(0, 0);

    // setup gestore segnali e timer
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = sighand;
    sa.sa_flags = SA_RESTART;
    sigaction(SIGALRM, &sa, NULL);
    setitimer(ITIMER_REAL, &itv, NULL);

    // titolo e sfondo
    glutCreateWindow("0");
    glewInit();
    glClearColor(1.f, 1.f, 1.f, 0.f);

    // visuale prospettica
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-1., 1., -1., 1., 0., 1.);
    // bordo (GL_LINE) o pieno (GL_FILL)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    // Abilito la terza dimensione
    glEnable(GL_DEPTH_TEST);
    glClear(GL_DEPTH_BUFFER_BIT);
    glDepthFunc(GL_LESS);

    glutDisplayFunc(display);
    glutMainLoop();

    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include <GL/glew.h>
#include <GL/freeglut.h>

typedef struct vertex
{
    float coord[4];
    float color[4];
} vertex_t;

typedef float mat4_t[16];


GLuint program;

vertex_t square[4] = {
        {{20, 20, 0, 1}, {1, 0, 0, 1}},
        {{80, 20, 0, 1}, {1, 1, 0, 1}},
        {{20, 80, 0, 1}, {0, 0, 1, 1}},
        {{80, 80, 0, 1}, {0, 1, 0, 1}},
};

static GLuint loadShader(char *fname, GLenum type)
{
    GLuint shader = glCreateShader(type);
    int fd = open(fname, O_RDONLY);
    GLint size = lseek(fd, 0, SEEK_END);
    GLchar *fd_map = mmap(NULL, size, PROT_READ, MAP_SHARED, fd, 0);
    
    glShaderSource(shader, 1, &fd_map, &size);
    glCompileShader(shader);

    close(fd);
    munmap(fd_map, size);

    return shader;
}

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT);

    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glutSwapBuffers();
}

static void init(void)
{
    GLuint vao, vbo;
    GLint proj_loc, model_loc;
    GLuint vert_shader = loadShader("vertex.vert", GL_VERTEX_SHADER);
    GLuint frag_shader = loadShader("fragment.frag", GL_FRAGMENT_SHADER);
    mat4_t proj_mat = {
            .02, 0, 0, -1,
            0, .02, 0, -1,
            0, 0, -1, 0,
            0, 0, 0, 1};
    mat4_t model_mat = {
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1};

    program = glCreateProgram();
    glAttachShader(program, vert_shader);
    glAttachShader(program, frag_shader);
    glLinkProgram(program);
    glUseProgram(program);

    glDeleteShader(frag_shader);
    glDeleteShader(vert_shader);

    glGenVertexArrays(1, &vao);
    glGenBuffers(1, &vbo);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof square, square, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof square[0], NULL);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof square[0],
                          (const void *)sizeof square[0].coord);
    glEnableVertexAttribArray(1);

    proj_loc = glGetUniformLocation(program, "projMat");
    glUniformMatrix4fv(proj_loc, 1, GL_TRUE, proj_mat);

    model_loc = glGetUniformLocation(program, "modelMat");
    glUniformMatrix4fv(model_loc, 1, GL_TRUE, model_mat);
}

static void resize(int w, int h)
{
    glViewport(0, 0, w, h);
}

int main(int argc, char **argv)
{
    // Setup OpenGL version
    glutInit(&argc, argv);
    glutInitContextVersion(4, 6);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(500, 500);
    glutCreateWindow("Hello");
    glutDisplayFunc(display);
    glutReshapeFunc(resize);

    glewInit();
    init();

    glutMainLoop();

    return 0;
}

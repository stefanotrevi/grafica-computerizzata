#version 460 core

layout(location=0) in vec4 sq_coord;
layout(location=1) in vec4 sq_color;

uniform mat4 projMat;
uniform mat4 modelMat;

smooth out vec4 color_in;

void main(void)
{
    gl_Position = projMat * modelMat * sq_coord;
    color_in = sq_color;
}

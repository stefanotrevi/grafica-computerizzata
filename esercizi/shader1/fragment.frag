#version 460 core

smooth in vec4 color_in;

out vec4 color_out;

void main(void)
{
    color_out = color_in;
}

#define _POSIX_C_SOURCE 200809L

#include <GL/glut.h>
#include <signal.h>
#include <stdio.h>
#include <sys/time.h>

#define MAX_SECONDS 4

static unsigned fps = 0U;

static void sighand(int sig)
{
    if (sig == SIGALRM)
    {
        // ISO 9899-2011 6.2.4
        static char buf[12]; // i32 usa max 12 char, inclusi segno e null byte
        static unsigned samp[MAX_SECONDS] = {0}, old_fps = 0U;
        static int sec = 0;
        unsigned old_samp = samp[sec = (sec + 1) % MAX_SECONDS];

        samp[sec] = fps - old_fps;
        old_fps = fps -= old_samp;
        snprintf(buf, sizeof buf, "%u", fps / MAX_SECONDS); // si può anche usare un float
        glutSetWindowTitle(buf);
    }
}

static void display(void)
{
    glClearColor(1.f, 1.f, 1.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT);
    glFinish();
    glutPostRedisplay();
    ++fps;
}

int main(int argc, char **argv)
{
    struct itimerval itv = {{.tv_sec = 1}, {.tv_sec = 1}};
    struct sigaction sa = {0};

    // setup finestra
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(0, 0);

    // setup gestore segnali e timer
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = sighand;
    sa.sa_flags = SA_RESTART;
    sigaction(SIGALRM, &sa, NULL);
    setitimer(ITIMER_REAL, &itv, NULL);

    // avvio UI
    glutCreateWindow("0");
    glClearColor(1.f, 1.f, 1.f, 0.f);
    glutDisplayFunc(display);
    glutMainLoop();

    return 0;
}

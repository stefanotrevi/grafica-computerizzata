#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif

#include <GL/glut.h>
#include <signal.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define __USE_MISC
#include <math.h>

static float randf(void)
{
    return (float)(rand() % 101) / 100;
}

static void display(void)
{
    time_t now = time(NULL);
    float pi = (float)M_PI;
    float s = now % 60 * pi / 30;
    float m = now / 60 % 60 * pi / 30;
    float h = now / 3600 % 24 * pi / 12;

    glClear(GL_COLOR_BUFFER_BIT);
    glBegin(GL_LINE_LOOP);

    for (int i = 0; i < 360; ++i)
    {
        float deg = i * pi / 180.f;
        glVertex2f(cosf(deg), sinf(deg));
    }

    glEnd();
    glBegin(GL_LINES);

    glColor3f(randf(), randf(), randf());
    glVertex3f(0, 0, 0);
    glVertex3f(sinf(s) / 1.25f, cosf(s) / 1.25f, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(sinf(m) / 1.5f, cosf(m) / 1.5f, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(sinf(h) / 1.75f, cosf(h) / 1.75f, 0);

    glEnd();

    // ho finito di disegnare
    glFinish();
    // swap dei buffer per il V-sync
    glutSwapBuffers();
    glutTimerFunc(1000, (void(*)(int))glutPostRedisplay, 0);
}

int main(int argc, char **argv)
{
    // inizializza glut
    glutInit(&argc, argv);
    // imposta tipo, dimensioni e posizione finestra
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(640, 480);
    glutCreateWindow("Finestra");
    glClearColor(0, 0, 0, 0);

    glutDisplayFunc(display);
    glutMainLoop();

    return 0;
}

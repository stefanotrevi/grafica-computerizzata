#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200809L
#endif

#include <GL/glut.h>

static void display(void)
{
    glClearColor(1, 1, 1, 0);
    glClear(GL_COLOR_BUFFER_BIT);
    // ho finito di disegnare
    glFinish();
}

int main(int argc, char **argv)
{
    // inizializza glut
    glutInit(&argc, argv);
    // imposta tipo, dimensioni e posizione finestra
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(640, 480);
    // crea la finestra
    glutCreateWindow("Finestra");
    glClearColor(1, 1, 1, 0);
    glutDisplayFunc(display);
    glutMainLoop();

    return 0;
}

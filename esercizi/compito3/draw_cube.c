#define _POSIX_C_SOURCE 200809L
#include <GL/glew.h>
#include <GL/glut.h>
#include <signal.h>
#include <stdio.h>
#include <sys/time.h>

#define NFACE 4
#define NSTRIP 4
#define NVERTEX 4

static int fps = 0;
static GLuint bufs[2], idxs[NFACE * NSTRIP * NVERTEX];

static const GLfloat cube[NFACE * NSTRIP * NVERTEX * 3] = {
        .0f, .0f, .0f, .0f, .2f, .0f, .6f, .0f, .0f, .6f, .2f, .0f,
        .0f, .2f, .0f, .0f, .6f, .0f, .2f, .2f, .0f, .2f, .6f, .0f,
        .4f, .2f, .0f, .4f, .6f, .0f, .6f, .2f, .0f, .6f, .6f, .0f,
        .2f, .4f, .0f, .2f, .6f, .0f, .4f, .4f, .0f, .4f, .6f, .0f,

        .0f, .0f, .0f, .0f, .2f, .0f, .0f, .0f, .6f, .0f, .2f, .6f,
        .0f, .0f, .0f, .0f, .6f, .0f, .0f, .0f, .2f, .0f, .6f, .2f,
        .0f, .2f, .4f, .0f, .6f, .4f, .0f, .2f, .6f, .0f, .6f, .6f,
        .0f, .4f, .0f, .0f, .6f, .0f, .0f, .4f, .6f, .0f, .6f, .6f,

        .6f, .0f, .0f, .6f, .2f, .0f, .6f, .0f, .6f, .6f, .2f, .6f,
        .6f, .0f, .0f, .6f, .6f, .0f, .6f, .0f, .2f, .6f, .6f, .2f,
        .6f, .2f, .4f, .6f, .6f, .4f, .6f, .2f, .6f, .6f, .6f, .6f,
        .6f, .4f, .0f, .6f, .6f, .0f, .6f, .4f, .6f, .6f, .6f, .6f,

        .0f, .0f, .6f, .0f, .2f, .6f, .6f, .0f, .6f, .6f, .2f, .6f,
        .0f, .2f, .6f, .0f, .6f, .6f, .2f, .2f, .6f, .2f, .6f, .6f,
        .4f, .2f, .6f, .4f, .6f, .6f, .6f, .2f, .6f, .6f, .6f, .6f,
        .2f, .4f, .6f, .2f, .6f, .6f, .4f, .4f, .6f, .4f, .6f, .6f};

static const GLubyte colors[NFACE * NSTRIP * NVERTEX * 3] = {
        255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0,
        255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0,
        255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0,
        255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0,

        0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0,
        0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0,
        0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0,
        0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255, 0,

        0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255,
        0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255,
        0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255,
        0, 0, 255, 0, 0, 255, 0, 0, 255, 0, 0, 255,

        255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0,
        255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0,
        255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0,
        255, 255, 0, 255, 255, 0, 255, 255, 0, 255, 255, 0};

//#define NO_VBO

static void sighand(int sig)
{
    char title[12];

    sprintf(title, "%d", fps);
    glutSetWindowTitle(title);
    fps = 0;
    (void)sig;
}

static void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPushMatrix();

    glTranslatef(0.5f, -1.2f, -.8f);
    glRotatef(180.f, 0.f, 1.f, 0.f);
    glRotatef(30.f, 0.f, 1.f, 0.f);

#ifdef NO_VBO
    for (int i = 0; i < NFACE; ++i)
    {
        const GLfloat *face = cube + (i * NSTRIP * NVERTEX * 3);

        glColor3ubv(colors + i);
        for (int j = 0; j < NSTRIP; ++j)
        {
            const GLfloat *strip = face + (j * NVERTEX * 3);

            glBegin(GL_TRIANGLE_STRIP);
            for (int k = 0; k < NVERTEX; ++k)
                glVertex3fv(strip + k * 3);
            glEnd();
        }
    }
#else
    for (int i = 0; i < NFACE; ++i)
        for (int j = 0; j < NSTRIP; ++j)
            glDrawElements(GL_TRIANGLE_STRIP, NSTRIP, GL_UNSIGNED_INT, (const void *)(i * NSTRIP * NVERTEX * sizeof *idxs + j * NVERTEX * sizeof *idxs));
#endif

    glPopMatrix();
    glFinish();
    //    glutPostRedisplay();
    ++fps;
}

int main(int argc, char **argv)
{
    struct itimerval itv = {{.tv_sec = 1}, {.tv_sec = 1}};
    struct sigaction sa = {0};

    // setup finestra
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(640, 480);
    glutInitWindowPosition(0, 0);

    // setup gestore segnali e timer
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = sighand;
    sa.sa_flags = SA_RESTART;
    sigaction(SIGALRM, &sa, NULL);
    setitimer(ITIMER_REAL, &itv, NULL);

    // titolo e sfondo
    glutCreateWindow("0");
    glewInit();
    glClearColor(1.f, 1.f, 1.f, 0.f);

    // Abilito i VBO
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    // Genero i VBO per le facce ed i colori del cubo
    glGenBuffers(2, bufs);
    glBindBuffer(GL_ARRAY_BUFFER, bufs[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cube) + sizeof colors, NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof cube, cube);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof cube, sizeof colors, colors);
    // Genero i VBO per gli indici
    for (GLuint i = 0; i < NFACE * NSTRIP * NVERTEX; ++i)
        idxs[i] = i;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufs[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof idxs, idxs, GL_STATIC_DRAW);
    glVertexPointer(3, GL_FLOAT, 0, NULL);
    glColorPointer(3, GL_UNSIGNED_BYTE, 0, (const void *)sizeof cube);

    // visuale prospettica
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-.2, .2, -.3, .1, .1, 5.);
    // bordo (GL_LINE) o pieno (GL_FILL)
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    // Abi
    glEnable(GL_DEPTH_TEST);
    glClear(GL_DEPTH_BUFFER_BIT);
    glDepthFunc(GL_LESS);

    glutDisplayFunc(display);
    glutMainLoop();

    return 0;
}
